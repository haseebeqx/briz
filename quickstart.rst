
.. _quickstart:

===========
Quick Start
===========
Let's create a simple two page website and a mobile application API for the website.
this is very simple when using Briz.

.. _installation:

Installation
------------
You can install Briz using composer which is the recommended way.

.. code-block:: bash

    $ composer create-project briz/briz quickstart
    
..

replace the quickstart with the path to the directory you want to install.
this will install all depandancies and make it ready to run. 

if you like the other way you can use  `git-repository <https://github.com/brizphp/briz>`_ . clone it or use download zip
option. then run the following command after navigating to the target directory 
(after extracting th zip if you did it in that way),

.. code-block:: bash

    $ composer install
    
..

.. _running:

Running
-------
You can easily run Briz navigating to ``www`` direcory and run the php Development Server.
assuming the code is at the directory quickstart, if you prefer other servers such as apache don't frogetpoint it to ``www``

.. code-block:: bash

    $ cd quickstart/www
    $ php -S localhost:8000
    
..

now navigate to ``http://localhost:8000`` using your favourite browser. you can see the Briz web page running.

.. _basic_steps:

Code
------
now use your favourite editor to open ``www/index.php``.
the default code in this file will be enough, since we are using the same code for this quickstart. let me explain,

web
___

.. code-block:: php

    <?php
    require '../vendor/autoload.php';
    $app = new Briz\App();
  
    $app->route("web", function($router){
        $router->get('/',function($briz){
            $data = 'hello';
            $briz->renderer('hello',['title'=>'Welcome to Briz Framework',
                'content'=>'Thanks for using Briz Framework. You can Learn More at Our Documentaion Page ',
                'link'=>'http://briz.readthedocs.org/en/latest/'
            ]);
        });
    });
    
    $app->run();     
    
..

this code will create a new Briz app and stores a reference to it in the ``$app`` variable. ``$app->route()`` function accepts
four arguments but only two is required. first argument is route name it is gven as "web". the second argument is a `callbak function <http://php.net/manual/en/language.types.callable.php>`_.
to which accepts we pass ``$router``.  now we can use ``$router``  to generate actual routes. 

``$router->get()`` function is used to create a Route with HTTP ``GET`` method. the first argument ``/`` indicates that
it will match with the url  ``http://localhost:8000``. you can find more on :ref:`routepatterns` section. the second parameter is a
function which accepts a bridge $briz. ``$briz->renderer()`` is a function to render using a renderer which determine which view class should be used for loading.
the first argument is ``hello``. which is the route file it will be stored as ``hello.view.php`` under ``MyApp/views/web``.
keep thid file as it is.

mobile
______

Now you can create a mobile API for the above website with very few lines of code. The router ``web`` in above code contains only one route.
even if it contains hundreds of routes we can create mobile part without much difficulty as long as you stick with ``renderer`` for rendering.
now add the following code before ``$app->run();`` . what we do here is making the route ``mobile`` as a child of route ``web``.
so it will inherit the properties of it's parent. and set an :ref:`identity`  ``header`` to it for constrining access.
and set the renderer to render results as Json.

.. code-block:: php

   $app->route('mobile',function($router){
	
	    // Identify using header identity.
	    // if a header With X-Request-Cli with value android encountered then it will use this router
	    $router->identify('header','X-Request-Cli','mobile');
	
	    //now the responses will be rendererd as json
        $router->setRenderer('JsonView');
    },'web');
    
..

Thats it. our website will render pages as json when acccessed with mobile. ``$router->identify()``  is used to provide an identity 
for the route. so, this will run only if certain conditions are satisfied. here we use a header identity which will match if there 
is a header with ``X-Request-Cli`` with the value ``mobile``. then we set a render to JsonView. which means renderer will output
everything as json. the last parameter ``web`` is the parent name.
now we have to run it. for that lets make an html page ``mobile.html`` to mock mobile by sending the required header.

.. code-block:: html
    
    <html>
    <body>
    <script>
    var request = new XMLHttpRequest();
    var path = "http://localhost:8000";
    request.onreadystatechange = function(){
    if(request.readyState == 4 && request.status == 200)
    {
        document.getElementById('yt').innerHTML = request.responseText;
    }
    };
    request.open("GET",path,true);
    request.setRequestHeader("X-Request-Cli","mobile");
    request.send();

    </script>
    <p id ="yt">loading...</p>
    </body>
    
..

This html code is just for sending the required request. i am not explaining it. it sends a request to localhost:8000 including 
header ``X-Request-Cli`` with value ``mobile``. open this page in browser to see the result yourself. this is a json response
our mobile device have to decode it and render it using mobile ui.

.. note::
    if ``mobile.html`` is not loading then it is due to Cross Origin policy in php development server.
    in that case add cors support or move ``mobile.html`` to ``www`` directory and access
    it using ``localhost:8000/mobile.html`` 

now your website is ready for both mobile and web. if accessed directly in browser it will provide text rich result. but when accessed using
``mobile.html`` it will send response as JSON.

If you want you can create an :ref:`identity` for a checking domain or sub domain and use it instead of header identity. multi platform is not only
the use of identity. you can create an identity for checking user roles and then create different routers for different users.
this will create a perfect seperation between users. and there are many other uses. visit :ref:`identity` to learn how to create an identity