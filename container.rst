
.. _container:

===================
Container Reference
===================

this section describes about how to use container and what data are available for you by default.

Using Container
===============

to add data to container use:

.. code-block:: php

    $this->container['key'] = 'value'

..

to get data from container you have two ways:
  
.. code-block:: php
    
    // access directly
    $value = $this->container['key'];
    
    //this method has an advantage of interoperability. so it is recommended.
    $value = $this->container->get('key');
    
..

to check if a key exists in a container:
  
.. code-block:: php
  
    if($this->container->has('key')){
        //do something
    }
    
..

Default data
============

the default data available in container are the following,

Keys set by ``Briz\App.php``
----------------------------

=============     =================================================================
key               Details
=============     =================================================================
``framework``     an instance of ``Briz\App``
``root_dir``      the root directory
``inherit``       an object containing details about parent and child relationship
``container``     a reference to container itself.
=============     =================================================================

Keys set by ``config/application.php``
--------------------------------------

========================      =================================================================================
Key                           Details
========================      =================================================================================
``app``                       Application namespace
``app_name``                  Application name
``controller_namespace``      Controller Namespace
``log_dir``                   Directory for logging errors
``display_errors``            if sets to true it will display errors ONLY IF application error_reporting is set
``output_chunk_size``         output chunk size when sending response.
``fake_method``               The request faking string.
========================      =================================================================================

Keys set by ``config/collections.php``
--------------------------------------

===================     =================================================================
key                     Details
===================     =================================================================
``routes``              a collection containing all routes.
``route_collector``     holds refernce to individual routes for each router.
``id``                  holds reference to all idenities registered.
===================     =================================================================

Keys set by ``config/view.php``
-------------------------------

======================     =================================================================
key                        Details
======================     =================================================================
``view_engine``            view engine namespace.
``default_view_class``     default renderer class for view.
``view_dir``               path to the directory where view details are stored.
======================     =================================================================

Keys set by ``config/providers.php``
------------------------------------

======================     =================================================================
key                        Details
======================     =================================================================
``server``                 Server parameters in $_SERVER stored as a collection.
``router``                 The route collection.
``request``                PSR-7 Request implementation by Briz.
``response``               PSR-7 Response implementation by Briz.
``logger``                 helper for logging.
``FakeMethod``             It will change the method of an request based on a head 
                           (see :ref:`fakingrequestmethod`)
======================     =================================================================

