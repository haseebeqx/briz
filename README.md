# Briz Framework Documentation #

This is repo containing Documentation source for Briz. for Briz Source Code Visit [Github Briz Page](http://github.com/brizphp/briz)

View documentation as html pages at [Briz Documentation website](http://briz.readthedocs.org)

This documentation uses reStructuredText format(.rst) and sphinx with php formatting.