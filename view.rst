.. _view:

====
View
====

views are the rendering part of Briz. it sets outout for sending response. renderer constructs response from views. where the viewfiles
are stored depends upon the viewEngine and views class used. the default view engine uses the configration from ``config/view.php`` 
to store the file. the default location is MyApp/views. the default view engine requires you to store it in a directory with the
same name as the router.

By default only the class ``ParsedView`` will use the view file configuration for view directory. it requies you to store files with 
a ``.view.php`` file extension. You can use other view engines like twig if you want.

ParsedView
----------

``ParsedView`` is a class which uses view directory. it checks if the view file exists and parses array or object to variables to
the file. it then stores it's output to response.

if we call renderer like 

.. code-block:: php

    $b->renderer('hello',['name'=>'haseeb','greet'=>'hello']);

..

if the name of the router is `web`. it will check inside ``views/web`` for the file ``hello.view.php`` and converts the array to variables.
so, we can use it in view file like ``$greet`` and ``$name``. if our view file contains,

.. code-block:: php

    <?php
    echo "$greet $name";
    ?>

..

it will output 

hello haseeb

JsonView
--------

the ``JsonView`` will generate output in JSON format and sets a Json content-type header. this will not use any files.

DirectView
----------

This view is not of much use. it will just write to response evrything thrown at it. it expects an array to be single dimentional. 

View Configuration
------------------

the view configurations should be stored inside ``config\view.php``

If you open this file you can find what are the values stored init.
