.. _providers:

=========
Providers
=========

Providers are the components used by a Briz application. if a provider is added to the system container it will be available everywhere in the application.
in Briz ``Request`` and ``Response`` components are providers.since they should be available everywhere in the system they are added
to the system container using ``config\providers.php`` file. everything in this file will be available inside container.
the ``logger`` in this file is an external component called monolog

With Briz you can easily add providers. Briz is designed for easily adding any components into the system. i call it as 
"just use it strategy". so you can use many external components easily.

Creating a Provider
===================

Since there is no default ``model`` in Briz, I decided to add it first. it is the M in any MVC framework (Model View Controller).
models are classes which should work with data. mainly they are used for communicating with database. since we dont have it by default,
it is just VC. so to make it MVC we are adding a model. 

there are two types of models which are widely used

    1. Active Record.
    2. Data mapper.
    
php-active-record and Eloquent are examples of active record type models. spot-2 and doctrine are examples of data mapper approch.

Both are good and have thier own advantages and disadvantages. you have to find it yourself. here i am going to add 
php active record as a provider in example.

Example
-------

Here we are going to add php active record as a provider. so that we will have an active record model implementation ready to use.
first find details about php active record from `Php active record website <http://www.phpactiverecord.org>`_ .
there is a quick start guide in that website. i am using the same connection code in that page bellow. but with few changes to pass arguments.

create a directory ``Providers`` inside ``MyApp``. where we will store all our providrs. and create an file ``PhpAR.php`` in it.

.. code-block:: php

    <?php
    namespace MyApp\Providers;
    
    use Activerecord;
    
    class PhpAR
    {
        /**
         * @param string $root root directory
         * @param string $app user directory name
         */
        public static function register($root,$app)
        {
            $cfg = ActiveRecord\Config::instance();
            $cfg->set_model_directory($root.'/'.$app.'/Models');
            $cfg->set_connections(
                array(
                    'development' => 'mysql://dev:dev@localhost/bridge',
                    'test' => 'mysql://username:password@localhost/test_database_name',
                    'production' => 'mysql://username:password@localhost/production_database_name'
                )
            );
            $cfg->set_default_connection('development');
        }
    }

..

here we created a class ``PhpAR`` and a static method ``register`` in it. next we have to find what values from container are needed
by this provider. here we will need to pass the location of the model directory. we have to store this value in ``MyApp/Models``. 
currently ``MyApp`` directory is at ``/home/projects/briz`` so i will have to use ``/home/projects/briz/MyApp/Models`` as path to models. 
to get this path we have to pass ``root_dir`` and ``app`` from container. so we defined two parameters ``$root`` and ``$app`` for
this function. the rest of the code is copy paste from the quick start at phpactiverecord website. you have to edit user name and
password. visit phpactiverecord website for more details

for this we need to have php active record library available in our application. for this we can use composer.

.. code-block:: bash

    $ composer require php-activerecord/php-activerecord
    
..

Now registering the provider,
if you want to register this provider globally. you can use ``config/providers.php`` file. it will return an array containing providers
to application initialization code which will add this to container.
there is a format for adding content to this array. 

``NameSpace\Class@StaticMethod@argument@argument2``

So, for our provider we will have to add ``"MyApp\Providers\PhpAR@register@root_dir@app"`` to this array.
here ``MyApp\Providers\PhpAR`` is the class with namespace. and ``register`` is the static method and ``root_dir`` and ``app`` are 
parameters from container passed to this provider. 

if you want it only available to a single controller. you can just load it inside that controller by passing arguments from container.
