.. Briz documentation master file, created by
   sphinx-quickstart on Sun Mar 27 23:47:57 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Briz framework 
==============================
Briz is an easy to use PHP framework designed to write powerful web applications ranging from simple api to large web applications.

Rapid to Develop and Quick to run
---------------------------------

.. image:: speed.PNG

It is faster than many micro frameworks out there. Even if you use most features like route inheritance, identity, controllers and views
(as in ``Briz loaded`` in the chart) it will still be faster than many popular micro frameworks. so dont worry about speed.

Fork Briz on `GitHub <https://github.com/brizphp/briz>`_ 

Features
========

Briz has a good set of features to help you.

Easy to Add Dependancies
------------------------
Briz uses dependancy injection. It is not that strict. so adding dependancies is very easy.

Introducing new Routing System 
------------------------------
Briz comes with a new Routing system. it helps in easily extending and seperating your web application. 
Route inheritance helps you to specify a route extends another route. this works just as in a programming language.

Identity
--------
Identity is about identifying a route from one another. this feature can also be used in other parts using a trait.

PSR-7 
-----
Briz supports PSR-7. You can work interoperably with any Psr-7 message implementations.

view :ref:`hello` for basic hello world examples or view :ref:`quickstart` for more details.

Basic Usage
-----------

.. code-block:: php

	require './vendor/autoload.php';
	$app = new Briz\App();
	$app->route("web", function($router){
		$router->get('/',function($b){
			$b->response->write('hello world');
		}
		$router->get('/{name}',function($b){
			$data = 'hello'.$b->args['name'];
			$b->response->write($data);
		});
	});
	$app->run();
..

Contents:


.. toctree::
   :maxdepth: 2

   GitHub <https://github.com/brizphp/briz>
   helloworld
   quickstart
   basics
   routing
   controller
   view
   identity
   providers
   container
   collections

.. toctree::
    :maxdepth: 2
    :caption: PSR 7 Reference
    
    PSR-7/request
    PSR-7/response
    PSR-7/uri
