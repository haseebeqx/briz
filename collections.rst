.. _collections:

===========
Collections
===========

A ``collection`` is a wrapper around an array. basic and most common operations on an array are implemented as methods in a collection.
in Briz collections uses ``Briz\Base\Collection`` class.

Adding an empty collection to Container
=======================================

If you want an empty collection available throughout the application from start. then you can add it to the container using 
``config/collections.php`` file. you can add a collection in this file by adding ``"name" => "init_collection"`` to the array.

adding non empty collection to container
========================================

To add a non empty container which should be available throughout the application you have to use it as a provider (see :ref:`providers`
section)

constructing
============

you can create a new collection by 

.. code-block:: php

    use Briz\Base\Collection;
    
    $collection = new Collection();
    
..

Creating from an array
----------------------

You can create a collection from an array by passing it to the constructor.

.. code-block:: php

    use Briz\Base\Collection;
    
    $array = [ 'name' => 'value','foo' => 'bar'];
    
    $collection = new Collection($array);

..

Adding/replacing
================

set
---

we can add content to a collection by using ``set`` method. if a key already exists it will get replaced by new value.

**Usage:**

.. code-block:: php

    $collection->set('key','value');

..

replace
-------

The ``replace($array)`` method will replace the collection with the elements of new array.


**Usage:**

.. code-block:: php

    $array = ['a'=>'b','c'=>'d'];
    
    $collection->replace($array);

..

merge
-----

To merge values an array with existing collection you can use ``merge($array)`` method.

**Usage:**

.. code-block:: php

    $array = ['a'=>'b','c'=>'d'];
    
    $collection->merge($array);
    
    //to merge two collections
    $newCollection = new Collection(['key'=>'value','foo'=>'bb','bar'=>'h']);
    $newCollection->merge($collection->all());
    

..


Get values
==========

get
---

``get($key, $default)`` method is used to get a value from a collection. ``$default`` is the value which we will use if there is no 
value available. if we don't set the ``$default`` the collection will return null if key doesn't exist.

**Usage:**

.. code-block:: php
    
    //If key not found it will return an empty array otherwise return the value
    $value = $collection->get('key',[]);
    
    //it will return null  if key is not in collection. otherwise will return the value.
    $value = $collection->get('key');

..

all
---

The ``all()`` method will return all key value pairs stored in the collection as an array.

**Usage:**

.. code-block:: php

    $value = $collection->all();

..

Remove Items
============

remove
------

To remove a single item you can use ``remove($key)``.


**Usage:**

.. code-block:: php

    $collection->remove('key');

..

clear
-----

``clear()`` will remove all items from the collection. then the collection will be an empty collection.

**Usage:**

.. code-block:: php

    $collection->clear();

..

Checking
========

has
---

The ``has($key)`` method is used to check if a key exists in the collection. 


**Usage:**

.. code-block:: php

    if($collection->has('key'))
    {
        //do something
    }

..

isempty
-------

check if a collection is empty with ``isempty()``

**Usage:**

.. code-block:: php

    if($collection->isempty())
    {
        //do something
    }

..

count
-----

get the number of items in a array


**Usage:**

.. code-block:: php

    $count = $collection->count();

..

Operations
==========

Each
----

The ``each($callback)`` method will return the collection after executing a callback function of each elements in the array.


**Usage:**

.. code-block:: php

    //callback to add prefix.
    $callback = function($key,$value){
            $key = 'rf_'.$key;
            $value = 'rf_'.$value;
    }

    $collection->each($callback);
..