.. _controller:

===========
Controllers
===========

controllers are a great way to keep code clean. From here onwards only 
controller class(not closure) will be treated as controllers and closure controllers 
will be specifically called closure controllers( see :ref:`basics` ). controllers helps to 
increase reusability of code. and it helps to group code better based on functionality

.. _controller_basics:

Basics
======

You must specify a controller directly from router or pass router to controller.

to specify a controller from router

.. code-block:: php

    $app = new App();
    $app->route('name',function($r){
            $r->get('/','IndexController');
            $r->get('/contact','IndexController@contact');
    },'parent',$args);
    
..

``IndexController@contact`` means the ``contact`` method at ``IndexController``

here a router instance is passed to ``$r`` and using ``$r->get()`` we set route ``/`` to ``IndexController``
since no method is specified it will use the default ``index`` method. next we set 
``/contact`` to ``contact`` method of ``IndexController``. ``@`` is the spererator to
identify controller from action (action is another name commonly used for methods inside
a controller)

Or we pass the controller to router.

.. code-block:: php
    
    $app = new App();
    $app->route('admin','AdminController');
    
..

here we passed the router to ``AdminController`` . more about controller routing is available at 
:ref:`controllerrouting` section.

Creating a controller
=====================
 
a controller must be inside controller namespace which is by default ``MyApp\Controllers`` 
if you look at ``MyApp/Controllers`` directory you can see a file ``ErrorsController`` Dont delete that
file. it is your responsibility to care that file. you can edit it in the way you want but dont delete it.

back to creating controller. the controller should extend ``Briz\Concrete\BController``. 
if you dont do so nothing is wrong. it will still work. but you will not get access to
some helper methods such as ``renderer()`` and ``show404()``

basic structure of a controller is as follows

.. code-block:: php

    namespace MyApp\Controllers;

    use Briz\Concrete\BController;

    class IndexController extends BController
    {
       /**
        * Index Page
        */
        public function index()
        {
           $this->renderer('index',['param'=>'value']);
        }
    
       /**
        * Contact page.
        *
        * @Use app
        */
        public function contact($app)
        {
            $data = $app;
            $this->renderer('index',['name'=>$app]);
        }
    }

..

this is our ``IndexController`` with the routes we defined above in :ref:`controller_basics` . index method will
match with the route ``/`` and contact method will match with the route ``/contact`` 
if used with the routes defined above. if you want to know what is the ``@Use`` in the 
docblock above the function ``contact``, read the section below.

Dependancy Injection and Passing Values
=======================================

Dependancies are the components used by the Briz. In Briz adding a dependancy is very easy.
dependancies are added using the config files in `config` directory. there is a container 
for storing all the dependancies. every value stored in this container can be accessed

consider there are two routes ``/profile`` and ``/profile/{name}``  for get method
(see :ref:`routepatterns`). 
which will point to the methods ``index`` and ``showProfile`` in the controller 
``ProfileController``

.. code-block:: php

    namespace MyApp\Controllers;

    use Briz\Concrete\BController;
    
    /**
     * Profile controller
     *
     * @Use auth
     */
    class ProfileController extends BController
    {
       /**
        * Index Page
        *
        * @Use membership
        */
        public function index($mem)
        {
           $min = $mem->type();
           $name = $this->auth->getname();
           $this->renderer('profile',['name'=>$name,'mem'=>$min]);
        }
    
       /**
        * show profile.
        *
        * @Use app_name
        */
        public function showProfile($app,$name)
        {
            $data = $app;
            $this->renderer('index',['name'=>$app,'user'=>$name]);
        }
    }

..

we can pass dependancies using ``@Use`` in docblock. it specifies which components 
should be loaded from container. by default you will have request and response dependancies injected.
you can pass more using ``@Use``. here we use two imaginary components ``auth`` 
and ``membership``. if we want a dependancy available everywhere in the class the @Use
can be used above the class as in the example above. in that case it will be in the form $component where `component` is
the key for the value stored in the container use it as ``$this->component`` inside a method. when the 
dependancy is only needed inside a method we can 
pass it using ``@Use`` above the method. in that case it will be passed to the arguments
in the function. if there are two injections then it will be passed to first two function
parameters in order. this is done by the internal ``ControllerResolver`` which resolves the controller for router.

the ``showProfile`` method has one parameter from ``@Use`` and one from ``/profile/{name}``
the parameter from route will be stored to $name in that method. which means the @Use parameters will be resolved before route parameters

there is no limit on what should be stored in container. here ``app_name`` is a string storing 
the application name. you can edit it inside ``config/application.php``.

.. note:: 

    if you dont want to use or don't like using ``@Use`` annotation to get values.
    you can simply use something like ``$mem = $this->container->get('membership')`` inside 
    the method. the ``container`` holds reference to all dependancies. you can access it
    using ``$this->container->get('key')`` method. more about this at :ref:`container`.

..

Available Methods
=================

The Following Methods are Available from BController class. if you create a new  ``Response`` object 
then there must a ``return`` statement with that ``Response`` or set ``$this->response`` to that object.

show404
-------

displaying 404 page. you can edit its default look inside ``ErrorsController``

.. php:method:: show404()
    
    Show a 404 response message
    
    :returns: ``Psr\Http\Message\ResponseInterface`` with 404 Page
    
usage

.. code-block:: php

    public function show($name)
    {
        if($this->notFound($name)
        {
            return $this->show404();
        }
    }
  
..

redirect
--------

.. php:method:: redirect($url,$code=302)

    redirect to a given url
    
    :param string $url: Url to redirect
    :param int $code: code to redirect.
    :returns: ``Psr\Http\Message\ResponseInterface`` with redirect
    
usage
 
.. code-block:: php

    public function show($name,$redirectUrl)
    {
        if($this->isAuthorized)
        {
            return $this->redirect($redirectUrl);
        }
    }
 
..

renderer
--------

use ther selected renderer to process the input to generate response

.. php:method:: renderer()
     
     minimum number of arguments is two. but  can have more than that based on number of responses.
     
     :param string $name: Name of the rendering
     :param array|object $params: Array or object containing  data in ``'key' => 'value'`` format
     :returns: ``Psr\Http\Message\ResponseInterface`` with processed output

Basic usage

.. code-block:: php

    public function show($name,$redirectUrl)
    {
        $params = $arrayOrObject;
        $this->renderer('hello',$params);
    }
 
..
