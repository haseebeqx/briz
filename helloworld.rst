
.. _hello:

================
Briz Hello world
================

Since Briz is a super flexible framework. you have many ways to write a hello world program.

install
=======

You need composer to install Briz.

.. code-block:: bash

    $ composer create-project briz/briz helloworld

..

this command creates briz inside the directory helloworld.

Basic Hello world
=================

This version uses ``echo`` instead of PSR-7 ``response`` object. edit ``index.php`` in  ``www`` directory.

.. code-block:: php

    <?php
    //autoload the required files
    require './vendor/autoload.php';
    
    // create a new application instance
	$app = new Briz\App();
    
    //create a router with the name web
    $app->route("web", function($router){
    
        //a route for GET requests to '/'
        
		$router->get('/',function($b){
			echo "Hello World";
		});
     });
    
    //run the application
    $app->run();
    
PSR-7 Hello World
=================

this version uses built in PSR-7 ``response`` object. edit ``index.php`` in  ``www`` directory.

.. code-block:: php
 
 <?php
    //autoload the required files
    require './vendor/autoload.php';
    
    // create a new application instance
	$app = new Briz\App();
    
    //create a router with the name web
    $app->route("web", function($router){
    
        //a route for GET requests to '/'
        
		$router->get('/',function($b){
			$b->response->write("hello world");
		});
     });   
    
    $app->run();

..
    
Hello World with Renderer
=========================

this version uses renderer so that we can use view. edit ``index.php`` in  ``www`` directory.
some framework features requires usage of renderer to work.

.. code-block:: php

    <?php
    //autoload the required files
    require './vendor/autoload.php';
    
    // create a new application instance
	$app = new Briz\App();
    
    //create a router with the name gen
    $app->route("gen", function($router){
    
        //a route for GET requests to '/'
        
		$router->get('/',function($b){
			$b->renderer('hello');
		});
     });   
    
    $app->run();

..

now create a directory ``gen`` (router name)  inside ``MyApp/views`` and create a file ``hello.view.php`` in it with any
content like this one,

.. code-block:: php

    hello world
..

HelloWorld with controller
==========================

.. code-block:: php

    <?php
    //autoload the required files
    require './vendor/autoload.php';
    
    // create a new application instance
	$app = new Briz\App();
    
    //create a router with the name gen
    $app->route("gen", function($router){
    
        //a route for GET requests to '/'
		$router->get('/','HelloController@sayHello');
     });   
    
    $app->run();

..

now create a file (controller) ``HelloController.php`` in ``MyApp/Controllers`` directory.

.. code-block:: php

    <?php
    namespace MyApp\Controllers;

    use Briz\Concrete\BController;
    class HelloController extends BController
    {
        public function sayHello()
        {
            $this->response->write("hello world");
        }
    }
    
..

more
====

usage of Briz framework for creating a web app and simple mobile api at the same time is explained in :ref:`quickstart`