.. _identity:

========
Identity
========

Identity is a global way for checking several conditions are satisfied. an identity will be given access to the Container. using this 
container an Identity can check for some conditions are satisfied or it can add values to the container.

for example, the default Identity included with Briz ``header identity`` checks if a header with a specific value exists in the request.
for doing this it accesses ``request`` from container and checks request headers. 

An identity should extend ``Briz\Base\Identity``.  and it must have a method ``identify()`` which will contain the logic. 
the container will be available as ``$this->container`` inside an identity. have a look at :ref:`container` for more details about container


Creating an identity
--------------------

for example lets create a simple identity for checking port number

create a directory ``Identities`` under MyApp and create a file ``PortIdentity.php``

.. code-block:: php

    <?php
    namespace MyApp\Identities;

    use Briz\Base\Identity;

   /**
    * Identity for checking port values.
    */
    class PortIdentity extends Identity
    {
        /**
        * Check for port.
        * 
        * @param int $port 
        * @return bool
        */
        public function identify($port, $v='')
        {
           $request = $this->container->get('request');
            if($request->getUri()->getPort() == $port){
                return true;
            }
            return false;
        }
    }

..

This code checks if a specific port $port is matched with the port number in the request.$v is initialized with blank value because
``IdentityInterface`` for identities requires two parameters for method ``identify()``.

at first, we need to register the identity. for this edit ``config\identities.php`` and add a key ``port`` and set its value to our class 
name with namespace. Then this file will look like,

.. code-block:: php

    <?php
    return [
        'header' => 'Briz\Beam\HeaderIdentity',
        'port' => 'MyApp\Identities\PortIdentity'
    ];

..

next in your :ref:`quickstart` file (or the default ``www/index.php`` when briz is installed ). edit the line for checking identity. 

.. code-block:: php

    //$router->identify('header','X-Request-Cli','mobile');
    
    $router->identify('port',8080,'');
    
..

just like above we had to pass a blank third parameter inside ``$router->identify()``. now use PHP Development Server to go to ``localhost:8000`` by

.. code-block:: bash

    $ php -S localhost:8000
    
..

and then after exiting, goto ``localhost:8080`` by typing

.. code-block:: bash

    $ php -S localhost:8080
    
..

check both and see the difference yourself. Now you have created your first Identity.

Using Identities outsite Route
------------------------------

you can also use identities outside the Route. for that, you have to use ``IdentityTrait``. As traits provide automated copy paste in php.
we can use ``IdentityTrait`` to add some methods and variables for accessing ``Identity`` to your Controller.

adding the trait.

.. code-block:: php

    class IdentityController
    {
        use \Briz\Beam\IdentityTrait;
    
        public function idcheck()
        {
            //code goes here.
        }
    }

..

the ``use \Briz\Beam\IdentityTrait;`` will add  the identity management trait for your controller.

now, you have the following methods and two ``protected`` arrays ``$identifies``, the list of identites registered with the class
and ``$idt_keyIndex``, the key index

addIdentity
___________

add a new identity.

this function can accept any number of arguments. you need to pass minimum two arguments.

.. php:method:: addIdentity(string  $name, mixed  $value)
    
    
    :param string $name: The name of the identity in container
    :param mixed $value: value to be identified
    :throws: \BadMethodCallException , \InvalidArgumentException

removeIdentity()
________________

Remove an added Identity.

.. php:method:: removeIdentity(string  $name, mixed  $key) 
    
    
    :param string $name: The name of the identity in container
    :param mixed $value: second argumet passed when AddIdentity was called.
    
removeAllIdentity()
___________________

if a name is specified it will only remove all identities under that name
    
.. php:method:: removeAllIdentity(string|null  $name = null)
    
    :param string|null $name: optional name.
    
identifyAll()
_____________

check if all added identities matches.

.. php:method:: identifyAll()

        :returns: boolean
        
identifyByName()
________________

identifies everything under a name if no key is specified otherwise check for identity matched by key.

.. php:method:: identifyByName(string  $name, mixed|null  $key = null)
    
    :param string|null $name: name of the identity.   
    :param mixed|null $key: second parameter when added Identity
    :returns: boolean