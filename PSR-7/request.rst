
.. _request:

=======
Request
=======

In Briz Request is a ``PSR-7`` implementation. it reprecents an incoming request received by the server. since it uses 
``Psr\Http\Message\ServerRequestInterface`` you can access and inspect any request parameters.

the request is available as ``request`` inside the container (see :ref:`container` ). this is loaded to controller by default. 
so it is available as ``$this->request`` inside controller. or you can get it from container as ``$container->get('request');``

from here onwards assume ``$this->request = $container->get('request');``

Request Method
===================

The HTTP request methods supported by Briz are,

    * CONNECT
    * DELETE
    * GET
    * HEAD
    * OPTIONS
    * PATCH
    * POST
    * PUT
    * TRACE
    
getMethod
---------

The ``getMethod()`` function is used to retrieve HTTP method of the request. it returns a string value reprecenting HTTP request method.

**usage:** 

.. code-block:: php

    $method = $this->request->getMethod();

..

withMethod
----------

With ``withMethod($method)`` you can create a copy or clone of the current request with another method.
so if you want to change the request method you have to use

.. code-block:: php

    $this->request = $this->request->withMethod($method);

..

where ``$method`` is a supported HTTP method.

.. _fakerequestmethod:

Faking request method
---------------------

Most browsers dosn't support any methods other than GET and POST.
so we can create a fake method. the default method faking string is ``X-HTTP-Method-Override``. you can edit it inside 
``config/application.php`` file. if this string is added in ``request header`` or ``request body`` it will override with the new one.

**Usage:**

.. code-block:: html

    <input type="hidden" name="X-HTTP-Method-Override" value="put">
    
..


Uri
===

There is an interface in PSR-7 for reprecenting Uri ``Psr\Http\Message\UriInterface``. So in request uri reprecents an object of a class
implementing that interface. more about our implementation is explained in :ref:`uri`

getUri
------

The ``getUri()`` method is used to retrieve the Uri object

**Usage:**

.. code-block:: php

    $uri = $this->request->getUri();
    
    //now you can use $uri object like,
    $port = $uri->getPort();
..

withUri
-------

The ``withUri($uri)`` method will create a request object with a new ``uri`` 

**Usage:**

.. code-block:: php

    //this will change the request Uri
    $this->request = $this->request->withUri($uri);

..

getRequestTarget
----------------

The ``getRequestTarget()`` method will retrieve our request target. if our uri is ``www.example.com/page/item/1?a=b`` the ```/page/item/1?a=b``
part is called request target. this method will retrieve this part as string.

**Usage:**

.. code-block:: php

    $target = $this->request->getRequestTarget();
 
..

withRequestTarget
-----------------

The ``withRequestTarget()`` method will create a clone of the request object with the specified string request target .

**Usage:**

.. code-block:: php

    $request = $this->request->withRequestTarget();
    
..

Server Parameters
=================

The server parameters part of the request are mostly derived from $_SERVER super global. but they can also contain other values.

getServerParams
---------------

The ``getServerParams()`` method is used to retrieve server parameters as an array.

**Usage:**

.. code-block:: php

    $params = $this->request->getServerParams();
    
    //then we can use it like $params['xyz'] etc.
    
..

Cookies
=======

getCookieParams
---------------

this method will retrieve request cookies as an array.

**Usage:**

.. code-block:: php

    $cookies = $this->request->getCookieParams();
    
..

withCookieParams
----------------

``withCookieParams(array $cookies)`` method will clone the request with the given array of cookies

**Usage:**

.. code-block:: php

    //this will change the request cookies
    $this->request = $this->request->withCookieParams($array);
    
..

Query String
============

consider the url ``http://example.com/briz/i?a=b&c=d`` the ``a=b&c=d`` part is called query string. query string parameters are
``a=b`` and ``c=d``

getQueryParams
--------------

The ``getQueryParams()`` method will retrieve query string parameters as an array.

**Usage:**

.. code-block:: php

    $queryParams = $this->request->getQueryParams();
    
..

withQueryParams
---------------

``withQueryParams(array $query)`` method will clone the request with given array of query params.


**Usage:**

.. code-block:: php

    //this will change the request query parameters
    $this->request = $this->request->withQueryParams($queryasArray);
    
..

Uploaded Files
==============

There is an interface in PSR-7 to reprecent UploadedFiles (``Psr\Http\Message\UploadedFileInterface``). so we will use our implementation
of this interface for reprecting uploaded files. Each uploaded File is reprecented by ``Briz\Http\UploadedFile`` class.

getUploadedFiles()
------------------

This method will retrieve an array of ``Psr\Http\Message\UploadedFileInterface`` objects reprecenting each uploaded file.

**Usage:**

.. code-block:: php

    $files = $this->request->getUploadedFiles();
    
..

withUploadedFiles
-----------------

The ``withUploadedFiles(array $uploadedFiles)`` method will create a clone of the current request with an array of uploaded files.

.. code-block:: php

    //this will change the uploaded files with current request
    $this->request = $this->request->withUploadedFiles($uploadedFiles);
    
..

Request Body
============

Http request body usually contains ``POST`` data. when you use HTTP POST method the request will send your data o server in the request body.
You can get this body directly or can get a parsed version (the data in ``$_POST`` is actually parsed version)

getBody
-------

The ``getBody()`` method is used to get unparsed HTTP body as it is available in pure HTTP message as a stream object.

.. code-block:: php

    // unparsed body
    $body = $this->request->getBody();
    
..

.. _requestgetparsedbody:

getParsedBody
-------------

The ``getParsedBody()`` method will retrieve a parsed body with respect to the content type header as string. the default content 
types supported are,

    ======================================      =====================
    Content Type                                        Return value
    ======================================      =====================
     ``application/x-www-form-urlencoded``      array
     ``multipart/form-data``                    array
     ``application/json``                       array
     ``application/xml``                        object
     ``text/xml``                               object
    ======================================      =====================

**Usage:**
    
.. code-block:: php

    $body = $this->request->getParsedBody();
    
..

registerParser
--------------

The ``registerParser($type, $callable)`` method will add a new parser for parsing current request body. it will override existing
parser if an existing content type is used as ``$type``. ``$callable`` is a callback function to parse http body. the callback function must return an 
array or an object. the callback function will get a string containing unparsed body as input.


**Usage:**


.. code-block:: php

    //parser to parse json as object instead of array.
    $parser = function($body){ 
            return json_decode($body);
       };
       
    $this->registerParser("application/json", $parser);
    
    $body = $this->getParsedBody();
    
..


withBody
--------

the ``withBody(StreamInterface $body)`` method will return an instance of request object with given stream object.

**Usage:**

.. code-block:: php

    $this->request = $this->request->withBody($newStream);
..


.. _requestheader:

Headers
=======

getHeaders
----------

The method ``getHeaders()`` Retrieves all Request header values as an array. 

**Usage:**

.. code-block:: php

    $headers = $this->request->getHeaders();
    
..


hasHeader
---------

``hasHeader($name)`` method checks if the current request has a header ``$name``. it will return a boolean value.

**Usage:**

.. code-block:: php

    if($this->request->hasHeader($name))
    {
        //do something.
    }
    
..

getHeader
---------

The ``getHeader($name)`` method returns header values for the header ``$name``. since an Http header can have multiple
values it returns an array.

**Usage:**

.. code-block:: php

    $header = $this->request->getHeader($name);
    
..

.. note:: 

    since some clients send multiple headers as comma seperated values. you may get only single
    value even if multiple header are there. a better alternative is to ``explode()`` ``getHeaderLine()``
    
..
 
getHeaderLine
-------------
 
The ``getHeaderLine($name)`` method will return a string containing comma seperated values of the header specified by ``$name``

**Usage:**

.. code-block:: php

    $header = $this->request->getHeaderLine($name);
    
..

withHeader
----------

The ``withHeader($name, $value)`` method will return an instance with the provided value replacing the specified header. please note 
that this will replace the header.

**Usage:**

.. code-block:: php

    $request = $this->request->withHeader($name, $value);
    
..

withAddedHeader
---------------

unlike ``withHeader`` the ``withAddedHeader($name, $value)`` will return an instance of request object with appending a header to the
current header.

**Usage:**

.. code-block:: php

    $request = $this->request->withAddedHeader($name, $value);
    
..

withoutHeader
-------------

it will return an instance of the request after removing a header

**Usage:**

.. code-block:: php

    $request = $this->request->withoutHeader($name);
    
..

.. _requestprotocol:

HTTP version
============

The http protocol version supported by briz are ``1.0``, ``1.1`` and ``2.0``

getProtocolVersion
------------------

The ``getProtocolVersion()`` method will return HTTP protocol version of the request.

**Usage:**

.. code-block:: php

    $version = $this->request->getProtocolVersion();
    
..

withProtocolVersion
-------------------

The ``withProtocolVersion($version)`` method returns an instance of current request with the given protocol version

**Usage:**

.. code-block:: php

    $request = $this->request->withProtocolVersion($version);
    
..