
.. _uri:

===
Uri
===

In PSR-7 URI is reprecented by an implementation of ``Psr\Http\Message\UriInterface``. we implement this as ``Briz\Http\Uri``.
according to PSR7 a uri is of the format ``scheme://authority/path?query#fragment`` or in more detail 
``scheme:[//[user:password@]host[:port]][/]path[?query][#fragment]``. but what we need in most cases is regular url like 
``scheme://host:port/path?query#fragment``

You can get the Uri object of current request from ``request`` inside the container.
from here onwards assume that:

.. code-block:: php

    $this->request = $this->container->get('request');
    $uri = $this->request->getUri();


Scheme
======

Uri schemes supported by Briz are ``HTTP`` and ``HTTPS`` or it can be blank as a placeholder. Other uri schemes will result in an 
``InvalidArgumentException``.

getScheme
---------

The method ``getScheme()`` will return the scheme 

**Usage:**

.. code-block:: php

    $scheme = $uri->getScheme();

..

withScheme
----------

The ``withScheme($scheme)`` method will return an instance of the Uri object with the given scheme.

**Usage:**

.. code-block:: php

    $uri = $uri->withScheme($scheme);

..

Host
====

A host can be an IP address or an address like ``example.com`` or long address like ``web.www.ex.example.com``

getHost
-------

The ``getHost()`` method will return a string host name.

**Usage:**

.. code-block:: php

    $host = $uri->getHost();

..

withHost
--------

The ``withHost($host)`` method will return an instance of the Uri object with the new Host.

**Usage:**

.. code-block:: php

    $host = $uri->withHost($host);

..

Port
====

a port number must be between 1 and 65535. the default port number of http is 80 and that of https is 443. If you are using 
a port number other than default one then you must specify it. ``http://example.com`` is actually ``http://example.com:80``. since 
80 is the default port of http it is nor given. the briz quick start tutorial will start php development server at port 8000.
so you have to specify it as ``http://localhost:8000`` since 8000 is not the default port.

getPort
-------

The ``getPort()`` method will return the port number as integer.

**Usage:**

.. code-block:: php

    $port = $uri->getPort();

..

withPort
--------

The ``withPort($port)`` method will return an instance of the uri object with the given port number.

**Usage:**

.. code-block:: php

    $uri = $uri->withPort($port);

..

Path
====

getPath
-------

gets the current uri path.

**Usage:**

.. code-block:: php

    $path = $uri->getPath();

..

withPath
--------

return an instance of current uri with given path.

**Usage:**

.. code-block:: php

    $uri = $uri->withPath($path);

..

Query String
============

The Query String is separated from the preceding part by a question mark.

getQuery
--------

The ``getQuery()`` method will return the query string 

**Usage:**

.. code-block:: php

    $query = $uri->getQuery();

..

withQuery
---------

The method ``withQuery($query)`` will return an instance of the Uri object with given query string.


**Usage:**

.. code-block:: php

    $uri = $uri->withQuery($query);

..

Fragment
========
the part of the Uri after ``#`` is called Uri Fragment. it is usually used as identifiers for sections in html page.

getFragment
-----------

The ``getFragment()`` method will return the Uri fragment as string.

**Usage:**

.. code-block:: php

    $fragment = $uri->getFragment();

..

withFragment
------------

The ``withFragment($fragment)`` method will return a clone of the Uri object with given fragment.


**Usage:**

.. code-block:: php

    $uri = $uri->withFragment($fragment);

..

User Information
================

Uri as defined by ``rfc3986`` and ``PSR-7`` can contain user information. user information is usename and password. empty passwords
are also valid.

getUserInfo
-----------

this method will retrieve ``username:password`` format string. if there is no password it will return ``username``. if there is no
usename or password it will return a blank string.

**Usage:**

.. code-block:: php

    $user = $uri->getUserInfo();

..

getAuthority
------------

The ``getAuthority()`` will return the URI authority, in ``[user-info@]host[:port]`` format.

**Usage:**

.. code-block:: php

    $authority = $uri->getAuthority();

..

withUserInfo
------------

``withUserInfo($user, $password)`` returns an instance of the Uri with the given user info. the password is optional.

**Usage:**

.. code-block:: php

    $uri = $uri->withUserInfo($user, $password);

..