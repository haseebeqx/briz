
.. _response:

========
Response
========

In Briz a ``response`` object implementing ``Psr\Http\Message\ResponseInterface`` is used to reprecent outgoing response. Briz 
will send this as response to client after processing. in briz default response object is ``Briz\Http\Response``

the response is available as ``response`` inside the container. so you can use ``$this->container->get('response');`` to get the current
response object. it is loaded inside the controller as ``$this->response``. 

This Page assume ``$this->response = $this->container->get('response')``

HTTP Status
===========

an HTTP response will contain a three digit status code which explains the status of the current response to the client. the most 
common HTTP status anybody know is ``404 Not Found`` since it is an error message. there are other status codes too.
in ``Briz\Http\Response`` status codes and reason phrases are defined as below.


.. code-block:: php

    protected static $phrases = [
        // INFORMATIONAL CODES
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        // SUCCESS CODES
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        // REDIRECTION CODES
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy', // Deprecated
        307 => 'Temporary Redirect',
        // CLIENT ERROR
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        // SERVER ERROR
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    ];
    
..

For normal response the status will be ``200 OK``. The codes above are the only codes supported by Briz. But you have an option to
change the reason phrase.

getStatusCode
-------------

The ``getStatusCode()`` will return the current status code set with the response object as integer.

**Usage:**

.. code-block:: php

    $status = $this->response->getStatusCode();

..

getReasonPhrase
---------------

The ``getReasonPhrase()`` method will return the current reason Phrase set with the response object

**Usage:**

.. code-block:: php

    $reason = $this->response->getReasonPhrase();

..

withStatus
----------

``withStatus($code, $reasonPhrase = '')`` method creates an instance or clone of current response object with new status code.
if optional ``$reasonPhrase`` is given it will change the reasonPhrase from default one.

**Usage:**

.. code-block:: php

    $reason = $this->response->withStatus($code);

..

Response Body
=============

Http Response body contains what you will get in browser window. it is the Html part of the response.

write
-----

the ``write($data)`` or ``setContent($data)`` will write data to output stream of briz response object which will be latter added to the
output stream of php after processing.


**Usage:**

.. code-block:: php

    $this->response->write($data);
    //OR
    $this->response->setContent($data);
..

getBody
-------

The ``getBody()`` method will return current response body as a stream object (``Psr\Http\Message\StreamInterface``).

**Usage:**

.. code-block:: php

    $this->response->getBody();
    
..

withBody
--------

the ``withBody(StreamInterface $body)`` method will return an instance of response object with given stream object.

**Usage:**

.. code-block:: php

    $this->response = $this->response->withBody($newStream);
..


Headers
=======

setHeader
---------

The ``setHeader($header, $value, $replace = true)`` will add a new http header. ``$header`` is the header name and ``$value`` is the
header value in string. if the optional ``$replace`` is set to ``false`` it will append the header without replacing it.

**Usage:**

.. code-block:: php

    //replace
    $this->response->setHeader($header,$value);
    //append
    $this->response->setHeader($header2,$value,false);
..

Other header methods
--------------------

other header methods are exactly same as the one in Request :ref:`requestheader`

HTTP Version
============

the supported operations on http protocol version are exactly same as the one in request :ref:`requestprotocol`
 