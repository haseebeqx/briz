.. _routing:

=======
Routing
=======

The basics of Routing in Briz is explaind at :ref:`routingbasics` section in Basics you can have a look at it.

Basic Routing
=============

The ``App::route()`` will create a new router with the name specified.
we can use this router to create routes. 

lets discuss about basic routing,

.. code-block:: php
    
    $app = new Briz\App();
    $app->route("web", function($router){
            //code
            $router->get('/','IndexController@show');
       });
    
..

that will create a router with the name ``web``. inside it you can specify routes. here ``$router->get('/','IndexController@show');`` is such a route

the second argument to the above method can be an anonymous function or a controller. if a controller is used the above
code will change to 

.. code-block:: php

    $app = new Briz\App();
    $app->route("web", "SomeController");

..

where ``SomeController`` is the controller name . if so routes will be generated from the controller. see :ref:`controllerrouting` below.
but i think the anonymous function version is better because it provides more flexibility. that was my personal opinion. you can use anything
you like.

GET Route
---------

The code shown above ``$router->get()`` is a get route.

.. code-block:: php

        $router->get('/details','IndexController@show');
        
..

This defines a GET route to the Controller ``IndexController`` and 
the method ``show`` in it. if this route matches to our url then it will execute the method ``show`` in ``IndexController``. 
if no method name is specified it will use ``index`` method by default.

if you are using a callback function instead of a controller you can use.

.. code-block:: php

    $router->get('/details',function($b){
		   $b->renderer('index',['title'=>'home']);
    }
    
..

POST Route
----------

.. code-block:: php

        $router->post('/','IndexController@create');
        
..

you can use POST routes when you have want to match a route with http POST method. see :ref:`requestgetparsedbody` in :ref:`request` for how 
to retrive post body.

PUT, DELETE, OPTIONS and PATCH
------------------------------

You can also create routes for PUT DELETE, OPTIONS and PATCH. but some of these routes may
not be supported by most of the browsers. so you will need an adapter to convert browser requests to these HTTP methods  
from a dummy method such as post. see :ref:`fakerequestmethod`.

.. code-block:: php
        
        //example of a put route
        
        $router->put('/','IndexController@edit');
        
        //example of a delete route
        
        $router->delete('/','IndexController@delete');
        
        //example of options route
        
        $router->options('/','IndexController@show');
        
        //example of patch route
        
        $router->patch('/','IndexController');
        
..

ANY
---

If you want a route to match all the HTTP methods specified above. then you can use  ``any`` method.

.. code-block:: php

        $router->any('/help','HelpController');
        
..

this will match any methods specified above. it will route to ``index`` method of
``HelpController``

Matching more than one method using set
---------------------------------------

You can also match multiple Http methods with ``set``

.. code-block:: php

        $router->set(['GET','POST'],'/help','HelpController');
        
..

the first parameter is an array specifying http methods. in the code above GET and POST methods are specified.

.. _controllerrouting:

Controller Routing
==================

It is possible to pass router directly to a controller. I am assuming you read :ref:`controller`
before reading this resource. 

when a router is directly passed to a Controller. the controller will have to generate 
routes.
for this we use Docblock. Here ``@Route`` is used to specify a route

.. code-block:: php

    <?php
    namespace MyApp\Controllers;

    use Briz\Concrete\BController;

    class AdminController extends BController
    {
       /**
        * @Route [get]
        */
        public function index()
        {
            $this->response->write('Administration Panel');
        }
    
       /**
        *
        * @Use app
        * @Route [get,post]
        */
        public function details($app,$n)
        {
            $this->renderer('details',[]);
        }
    }
    
..

here the comma seperated values in ``@Route`` inside ``[]`` in the doc block is used as HTTP methods to resolve routes. 
these routes are resolved as ``/admin`` since the first method name is ``index`` and ``/admin/details/{name}`` . the first part is the name of the controller before ``Controller``
in lower case. here it is ``admin``. the second part is the name of the method. and last part is parameters passed to it. if there are ``n`` number of ``@Use`` anotation
then first ``n`` parameters will be discarded. here it discards the ``$app`` in ``details`` this way

Route Inheritance
=================

The first parameter passed to  ``$app->route()`` is route name.  the second parameter 
is controller and the third parameter is parent. the parent should be name of another
router.

a `child` route will have all the properties of its `parent`. including routes and
identities. as stated in :ref:`quickstart` the main advatage of this is to extend 
current route. 

if a child route matches then its parent will be ignored.

.. _routepatterns:

Route Patterns
==============

.. code-block:: php

    //static route matches only /hello
    $route->get('/hello','handler');
    
    //dynamic route matches with hello/*
    $route->get('/hello/{name}','handler');
    
    // Matches /user/42, but not /user/xyz
    $r->get('/user/{id:\d+}', 'handler');

    // Matches /user/foobar, but not /user/foo/bar
    $r->get('/user/{name}', 'handler');

    // Matches /user/foo/bar as well
    $r->get('/user/{name:.+}', 'handler');

    // This route
    $r->get('/user/{id:\d+}[/{name}]', 'handler');
    // Is equivalent to these two routes
    $r->get('/user/{id:\d+}', 'handler');
    $r->get('/user/{id:\d+}/{name}', 'handler');

    // This route is NOT valid, because optional parts can only occur at the end
    $r->get('/user[/{id:\d+}]/{name}', 'handler');
    
..