.. _basics:

======
Basics
======

.. _routingbasics:

Routing
=======

in modern web frameworks all requests will be handled by a router.
in our case there are two files in www folder 1 is an .htaccess file and other is an index.php file
all requests to this web server will come to the access point index.php since it is the only web accessable part. 
so without .htaccess file our typcal request will be ``localhost:8000/index.php/hello`` but our .htaccess file 
passess all requests to index.php if there is no file extension such as .php .js etc. so we will have to use only 
``localhost:8000/hello`` instead of ``localhost:8000/index.php/hello``. 

well, that is not the routing we are talking about. routing is done by router component. it generates appropriate responses based on the 
request for this it uses the url after ``localhost:8000`` here it is ``/hello``. so if create a route for ``/hello`` 
it defines what should we do when our request reaches ``localhost:800/hello``. 

you can find more about routing at :ref:`routing` page.

.. _controllerbasics:

controller
==========

controllers controlls the route. it is the main component of the system. when a request is recived by index.php it is passed to the 
router. router finds an appropriate controller to do actions related to the path in the url. and then this controller decides what to do.
in Briz you can pass set a controller in two ways. as a callback function or as a Controller class. the 

.. code-block:: php

    //Using Controller as a callback
    $router->get('/{name}',function($briz){
		    $briz->render('hello',['name' => 'briz->']);
    });
    
    //Using Controller as a class
    $router->get('/profile/{name}','ProfileController@show');
    
''

when using a controller as a callback you are using can use passed $briz to handle rendering. then when using controller as a class 
it should be stored in the controller namespace. by default it is at ``MyApp\controllers`` so you should store it there.
the ``ProfileController@show`` shows that we should use the public method show in the controller class named ProfileController
 
we have another options of using to directly define route in ``$app->route()`` that will be explained in :ref:`routing` section.
more on controllers can be found at :ref:`controller` page.
 
Views
=====

Views are all about how the data  is sent to the user. in a typical MVC workflow the application logic is seperated from view. 
we pass the output generated from controller to the view. The renderer is responsible for rendering the view. view_engines or
renderers decide how to process the input. the default view_engines consists of components like ParsedView and JsonView for rendering 
html and JSON respectively. 